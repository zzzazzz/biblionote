<?php
/**
 * Fonctions du squelette associé
 *
 * @plugin     Biblionote
 * @copyright  2021
 * @author     Fa_b
 * @licence    GNU/GPL v3
 * @package    SPIP\Biblionote\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


// pour initiale et afficher_initiale
include_spip('prive/objets/liste/auteurs_fonctions');
