<?php

/**
 *  Fichier généré par la Fabrique de plugin v7
 *   le 2021-02-25 16:51:55
 *
 *  Ce fichier de sauvegarde peut servir à recréer
 *  votre plugin avec le plugin «Fabrique» qui a servi à le créer.
 *
 *  Bien évidemment, les modifications apportées ultérieurement
 *  par vos soins dans le code de ce plugin généré
 *  NE SERONT PAS connues du plugin «Fabrique» et ne pourront pas
 *  être recréées par lui !
 *
 *  La «Fabrique» ne pourra que régénerer le code de base du plugin
 *  avec les informations dont il dispose.
 *
**/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$data = array (
  'fabrique' => 
  array (
    'version' => 7,
  ),
  'paquet' => 
  array (
    'prefixe' => 'biblionote',
    'nom' => 'Biblionote',
    'slogan' => 'Pour créer des listes bibliographiques simples',
    'description' => 'Pour éviter d\'installer un plugin trop lourd de type zotero, et pouvoir afficher comme pour un blog, une liste de références (avec ou sans lien). Pensez à augmenter ce plugin avec les mots-clés.',
    'logo' => 
    array (
      0 => '',
    ),
    'credits' => 
    array (
      'logo' => 
      array (
        'texte' => 'Freepik',
        'url' => 'https://www.flaticon.com/free-icon/book_4052389?term=book&related_id=4052389',
      ),
    ),
    'version' => '1.0.3',
    'auteur' => 'Fa_b',
    'auteur_lien' => '',
    'licence' => 'GNU/GPL v3',
    'categorie' => 'divers',
    'etat' => 'experimental',
    'compatibilite' => '[3.0.0;3.2.*]',
    'documentation' => '',
    'administrations' => 'on',
    'schema' => '1.0.2',
    'formulaire_config' => '',
    'formulaire_config_titre' => '',
    'inserer' => 
    array (
      'paquet' => '',
      'administrations' => 
      array (
        'maj' => '$maj[\'1.0.2\'] = array(array(\'sql_alter\',\'TABLE spip_biblionotes CHANGE titre titre VARCHAR(128) NOT NULL DEFAULT ""\')); ',
        'desinstallation' => '',
        'fin' => '',
      ),
      'base' => 
      array (
        'tables' => 
        array (
          'fin' => '',
        ),
      ),
    ),
    'scripts' => 
    array (
      'pre_copie' => '',
      'post_creation' => '',
    ),
    'exemples' => '',
  ),
  'objets' => 
  array (
    0 => 
    array (
      'nom' => 'biblionotes',
      'nom_singulier' => 'biblionote',
      'genre' => 'feminin',
      'logo' => 
      array (
        0 => '',
        32 => '',
        24 => '',
        16 => '',
        12 => '',
      ),
      'table' => 'spip_biblionotes',
      'cle_primaire' => 'id_biblionote',
      'cle_primaire_sql' => 'bigint(21) NOT NULL',
      'table_type' => 'biblionote',
      'champs' => 
      array (
        0 => 
        array (
          'nom' => 'Titre',
          'champ' => 'titre',
          'sql' => 'varchar(128) NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
            2 => 'obligatoire',
          ),
          'recherche' => '6',
          'saisie' => 'input',
          'explication' => 'Titre de l\'ouvrage, site, magazine',
          'saisie_options' => '',
        ),
        1 => 
        array (
          'nom' => 'Lien',
          'champ' => 'lien',
          'sql' => 'tinytext NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '4',
          'saisie' => 'input',
          'explication' => 'Lien vers la ressource (site, éditeur…)',
          'saisie_options' => '',
        ),
        2 => 
        array (
          'nom' => 'Auteur(s)',
          'champ' => 'auteur',
          'sql' => 'varchar(25) NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '7',
          'saisie' => 'input',
          'explication' => 'Ex: Lorentz (Philippe) et Sandron (Dany)',
          'saisie_options' => '',
        ),
        3 => 
        array (
          'nom' => 'Éditeur',
          'champ' => 'editeur',
          'sql' => 'varchar(25) NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '4',
          'saisie' => 'input',
          'explication' => 'Nom de l\'éditeur',
          'saisie_options' => '',
        ),
        4 => 
        array (
          'nom' => 'Lieu de la publication',
          'champ' => 'lieu',
          'sql' => 'varchar(25) NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '2',
          'saisie' => 'input',
          'explication' => 'Concerne principalement l\'édition d\'ouvrages',
          'saisie_options' => '',
        ),
        5 => 
        array (
          'nom' => 'Annee',
          'champ' => 'annee',
          'sql' => 'varchar(25) NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '2',
          'saisie' => 'input',
          'explication' => 'Année de l\'édition de l\'ouvrage',
          'saisie_options' => '',
        ),
        6 => 
        array (
          'nom' => 'Commentaire',
          'champ' => 'commentaire',
          'sql' => 'mediumtext NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '5',
          'saisie' => 'textarea',
          'explication' => '',
          'saisie_options' => '',
        ),
      ),
      'champ_titre' => 'titre',
      'champ_date' => '',
      'champ_date_ignore' => '',
      'statut' => 'on',
      'chaines' => 
      array (
        'titre_objets' => 'Biblionotes',
        'titre_page_objets' => 'Les biblionotes',
        'titre_objet' => 'Biblionote',
        'info_aucun_objet' => 'Aucune biblionote',
        'info_1_objet' => 'Une biblionote',
        'info_nb_objets' => '@nb@ biblionotes',
        'icone_creer_objet' => 'Créer une biblionote',
        'icone_modifier_objet' => 'Modifier cette biblionote',
        'titre_logo_objet' => 'Logo de cette biblionote',
        'titre_langue_objet' => 'Langue de cette biblionote',
        'texte_definir_comme_traduction_objet' => 'Cette biblionote est une traduction de la biblionote numéro :',
        'titre_\\objets_lies_objet' => 'Liés à cette biblionote',
        'titre_objets_rubrique' => 'Biblionotes de la rubrique',
        'info_objets_auteur' => 'Les biblionotes de cet auteur',
        'retirer_lien_objet' => 'Retirer cette biblionote',
        'retirer_tous_liens_objets' => 'Retirer toutes les biblionotes',
        'ajouter_lien_objet' => 'Ajouter cette biblionote',
        'texte_ajouter_objet' => 'Ajouter une biblionote',
        'texte_creer_associer_objet' => 'Créer et associer une biblionote',
        'texte_changer_statut_objet' => 'Cette biblionote est :',
        'supprimer_objet' => 'Supprimer cette biblionote',
        'confirmer_supprimer_objet' => 'Confirmez-vous la suppression de cette biblionote ?',
      ),
      'liaison_directe' => '',
      'table_liens' => 'on',
      'vue_liens' => 
      array (
        0 => 'spip_mots',
      ),
      'afficher_liens' => 'on',
      'roles' => '',
      'auteurs_liens' => '',
      'vue_auteurs_liens' => '',
      'fichiers' => 
      array (
        'echafaudages' => 
        array (
          0 => 'prive/squelettes/contenu/objets.html',
          1 => 'prive/objets/infos/objet.html',
          2 => 'prive/squelettes/contenu/objet.html',
        ),
        'explicites' => 
        array (
          0 => 'action/supprimer_objet.php',
        ),
      ),
      'autorisations' => 
      array (
        'objets_voir' => '',
        'objet_creer' => '',
        'objet_voir' => '',
        'objet_modifier' => '',
        'objet_supprimer' => '',
        'associerobjet' => '',
      ),
      'boutons' => 
      array (
        0 => 'menu_edition',
        1 => 'outils_rapides',
      ),
    ),
  ),
  'images' => 
  array (
    'paquet' => 
    array (
      'logo' => 
      array (
        0 => 
        array (
          'extension' => 'png',
          'contenu' => 'iVBORw0KGgoAAAANSUhEUgAAAgAAAAIACAYAAAD0eNT6AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAOxAAADsQBlSsOGwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAACAASURBVHic7d13eFRVwsfx35RMCknoHaRJx4KLCApiW3tFxa7ruiurrmLfVXQtu7ZXX3XXtaxt115YxcXuinTktdE7hA4CCYEkkGSSmXn/GESRlty5d87MnO/nefI8q+ac+5vZJPc3t5zrE0xpLqm7pB6Sum3/aiYpX1KBpIbb/3e2qYAAkAaqJW2VtEnSSkkLJc2VNF7SPEkxY8lSnM90AIv0kHSMpKMlHSmphdk4AJDx1kv6QNLLkiaJMrATCoB3ApKOk3SBpF9KamM2DgBYbamkhyX9S/GjBtajALivj6RLJV0kdvoAkGrWSLpD0kuy/IgABcAdPkmnSbpN0gDDWQAA+zZZ0nDFrxOwkt90gDTnV3zH/7Wk/4idPwCki0GSvpU0wnQQUzgC4Nzpip9P6mY6CAAgIf+QdI2kiOkgyUQBqL92kh6TdI7pIAAA14yRdL6kStNBkoUCUHfZkm6WdLukPMNZAADu+1jSGZJqTAdJhoDpAGmio6QPFb+6P8tsFACAR7pK6iLpPVlwhwAFYN+GSvpI0v6mgwAAPHeApCrF7xLIaJwC2LMcSQ9Jus50EABAUtUqvnLrJNNBvEQB2L0mkt6XdLjpIAAAI5ZL6i1pm+EcnuEUwK5aS/qvpENNBwEAGNNI8Q/JX5gO4hWOAOysh6RPJHUwHQQAYFxYUk9JRaaDeIEC8KMDFG96zZK1waDfr77NW6lX0+bav1FTdW3cRB0KGqlxYQM1a5Sv/FC2soPBZMUBgLSzpapKa8vLVFRaqg8XL9CYhfO1pqzMzU08J+lKNydMFRSAuI6SpigJD+/p2ripTu64v4a066gj2rRXg6zQjv/m8/uUnZ+jQIgzMwDgRCQa1Uszv9Nd48ZqddkWN6YMK35r4Go3JkslFACpueK3e3i2pG/jnFyd07WXLujeR/1btd3t9wSyAsouyJbPz+MZACBR22pqdOX7o/XarBluTPcnSX92Y6JUYnsBKFD8sH8/LyZvm1+gEX0H6Fe9D1ZecM/rBwVCQeUU5PD/BgC47L6J43THF/9NdJrFysDnvth8rNkn6W1JR7s9ccu8Bnpg0HF65thTNaB1O2X59/w2Z+VkKZudPwB44sgOnbSpcpu+WpPQEfymit8avs6dVKnB5gJwg1xe5Cfg8+nKA/vp9ZPO1sDW7RXYx+H8rLyQQg2y3YwAAPiZX3bpqnHLirRyy+ZEpilS/FqxjGHr587DJE2UFNrXN9ZV98bN9PwvT1ffFq3q9P1ZOVkK5bPzB4Bk+GbtGvV/9inFnC/x/6mkE12MZJyNV5w1lvSmXNz5X9jjAE0cdnmdd/7B7CA7fwBIon5t2mpor96JTPELt7KkChsLwN8Uv+0vYaFAQE8dc4qePe40Nciq20MCA6GAstn5A0DSXXzgwYkMb6b4tQAZw7YCcJSki9yYqEFWSKNOGaZLex1U5zG+gE/ZBbmSz9YzLwBgzvFduiq3jh/W9iCj7gSwqQAEFf/0n/Det3FOrsaccYGO3a9T3Qf5fMopyGXfDwCG5GVlqVfzFolMwRGANHWT4sv9JqQglK0PzrhQh+1hQZ89CeWF5A/a9HYDQOppU1CQyPCEBqcaW/ZIbSTdmegkOYGgRp1yrg5q3rJe4wKhoLJyWNMfAExr0SA/keEJDU41thSAmyQ1SGQCv8+nF084Q4Pa7le/gT6fshuEOO8PACkgmNhy6xm1z8yoF7MHTeXCk5xuOGSATu/cvd7jQnlZ8gVseJsBAOnEhj3TDUrwsE3/Vm11x2FD6j3OH/ArK8e15QYAAHBNpheAhpKuSWiC7By9fOJZynJw2CirQcjetRYBACkt0wvApZIaJTLBXQOGqF1+Yb3H+QN+BUNc+AcASE02FADH+rZopSv6HOJobFYeh/4BAKkrkwtAL0n9nA72SXp0yIkKOLh63+f3Kxiy+UGLAIBUl8kF4JJEBp/YcX8d2rKNo7FZOUFu+wMApLRMLQB+SRcnMsHN/Y5wPDaYndBa0wAAeC5TC0BfSe2cDj6yXYd6L/X7g0BWQL4An/4BAKktUwvAMYkMvrKP88c+B3P49A8ASH2ZWgCOdjqwMJStEzru72ywz6cAt/4BANJAJhaAoKRBTgcP69ZbuUFnO3F/wMe1fwCAtJCJBaC/Enhk49ldezrecCCLT/8AgPSQiQXA8Qn8vGCW+rdyfO2gAqFMfDsBAJkoE/dY9X9k33YDWrdTdsD5Aj6BIIv/AADSQyYWgG5OBx7ZroPjjfr8fhb/AQCkjUwsAD2cDjykRWvHG/UH2fkDANJHphWABkpgAaCujZo63rDfweOCAQAwJdP2Wm0Vf45PveUFs9Q23/HNA/IFMu2tBABkskzbazneg3du2Fj+BM7h+/ycAgAApI9MKwCFTgc2yc1NaMM+LgAEAKSRTCsA+U4HFmSFEtsyRwAAAGkk0wqA41MADRIsAOz+AQDpJNMKQAPHA7MSe4ofZwAAAOkk0wqA49eTyAWAcTQAAED6yLQCAAAA6oACAACAhSgAAABYiAIAAICFKAAAAFiIAgAAgIUoAAAAWIgCAACAhSgAAABYiAIAAICFKAAAAFgoaDoA0lNNJKLxy5fpi2VL9d26tVq8qVibq6q0papK0VjMdDwg4/h9PjXMyVHjnFx1bdpUh7Ruq2M6ddaQDp2UFQiYjoc0RAFAvawtL9NjX07RyzOna8PWCtNxAGtEYzGVVlaqtLJSRaWb9OmSxXpg0ni1aJCvyw7uqxsGDFLrAsdPRIeFKACok201Nbp/0ng9MnWSqmtrTccBsN2GrRV6eMokPfF/X+qWI47UbYOGKDfBx5vDDhQA7NP8jRs0bNQbmrNhvekoAPagqrZWf57whd6eO0tvn3uhDmzZynQkpDguAsRejS1aqsOef5qdP5AmFhYX6/AXntHHixeZjoIURwHAHn24aKFOeu1fKq+uNh0FQD1sDYd1xpuvUAKwVxQA7NZ369Zq2KjXVROJmI4CwIGaSETnjnpdM75fZzoKUhQFALsor67WeaPe0LaaGtNRACRgazis80a9oYpw2HQUpCAKAHZx1/jPtWRTiekYAFywqKRY904YazoGUhAFADtZsqlET/zfl6ZjAHDR49OmallpqekYSDEUAOzk4SmTVBuNmo4BwEU1kYj+Z8pE0zGQYigA2GFrOKzXZ88wHQOAB16dNZ1rAbATCgB2+GDRAv5AABmqIhzWJ0u4LRA/ogBgh7HLlpqOAMBDY4v4HcePKADY4es1q01HAOChr9fyO44f8SwA7LA4gVv/urVuqyd/c5UO69pdoWD9f6yieQ0kv8/x9oFMF66t1bT5c3XtU49rscOyvqik2OVUSGccAYCk+INEtiZw/v+V627S4J69He38JfGTCOxDKBjUkQccpJduvt3xHOXV1TzNEzvwZxeSlNDOvzA3Twd16OR84z6fJD79A3VxcJeuKszLczyeC33xAwoAJEnRWMzx2FAWZ5KAZAplZTkem8jvOjILBQAAAAtRAAAAsBAFAAAAC1EAAACwEAUAAAALUQAAALAQBQAAYA2/L6E1RyJu5UgFFAAAgDWa5zVIZPh6t3KkAgoAAMAafVu3cTo0JmmGi1GMowAAAKxxfJeuapKb62ToVEmrXI5jFAUAAGCNvKwsjTzy6PoOi0ka6UEcoygAAACrXD/gCJ3Ro1d9htwtaYI3acyhAAAArOL3+fT2uRfod/0Ok2/vTyKtlnS9pHuTkyy5KAAAAOuEAgE9feoZmvbbq3TZwYeoTUHhD/+pVtISSY9L6i7pr6Yyeo3nuAIArNW/bTv1b3uOJKm6tvb7nL/8qa2kqNlUycERAAAAJGUHgzFZsvOXOAIAyy1es1oTZs3Quk0lCtfWmI6DJAgFs9S6SVMddVBf7d+mrek4gDEUAFhpZtES3fr805o0Z5bpKDDoyAMO0v9ccZUO7NzFdBQg6TgFAOuMnjJRR986gp0/NHH2TB196wi9N3WS6ShA0lEAYJVpC+bq148+qKpw2HQUpIjKcLUu/98H9NXC+aajAElFAYA1orGYbnjm76qu4Vw/dlZdU6Nrn3pc0VjMdBQgaSgAsMbE2TM1s2iJ6RhIUbOXFWkyp4VgEQoArDF2+jemIyDFfc7PCCxCAYA1Vm7YYDoCUtzKDRn1uHdgrygAsIbfv9c1vwEF/PxJhD34aYc1OrZsZToCUlzHlq1NRwCShgIAa5zQr7/pCEhx/IzAJhQAWGNAj94a2LO36RhIUUf0PkD9u/c0HQNIGgoArPK3q0coPyfXdAykmILcPP3tqutMxwCSigIAq/Tu0ElvjbxHjfLzTUdBimiUn6+3Rt6jnvt1NB0FSCoKAKxz9EF9NfWxp3Xu4KMVDARMx4EhwUBAw448Wl8+/oyOOvBg03GApONpgLBSx5at9NItt+vx8ms1ee4srS7eqG3V1aZjIQnysrPVrllzDep9oBoXFJiOAxhDAYDVGhcU6LQBR5iOAQBJxykAAAAsRAEAAMBCFAAAACxEAQAAwEIUAAAALEQBAADAQhQAAAAsRAEAAMBCLAQEALDWlqoqvTV3lj5bukQrNm9uJmmSpCJJH0h6T1KN0YAeogAAAKz08szpuunTj1S8besP/ypL0qDtX5dKWiTpCkmTzST0FqcAAADW+cvEcbps9Kif7vx3p5uksZJOT06q5KIAAACsMmbhfP3pi8/r+u0hSa9J2t+7RGZQAAAA1ohEo7r5s48UU6w+w/Il3edRJGMoAAAAa0xeuUKLS0qcDD1LUmOX4xhFAQAAWGPKqhVOh2ZJ6u9iFOMoAAAAa6wrL09keBu3cqQCCgAAwBo10Ugiw0Nu5UgFFAAAACxEAQAAwEIUAAAALEQBAADAQjwLANb6cv5cvfjph5owa4a+L92k2khCFwchTQQDAbVq3ERHHdhXl59wsgb27G06EmAEBQDWqQxX6/d/f0xvjB9rOgoMqI1EtLp4o1794jO9+sVnuvDo4/TENdcrN5RtOhqQVBQAWCVcW6uz7hmpibNnmo6CFPH6uM+1prhYY+59QFkB/iTCHlwDAKv8+bV/sfPHLibMnqH7Xn/ZdAwgqSgAsMb60k168v3RpmMgRT3xn3e1cctm0zGApKEAwBrvT5uqqnDYdAykqMpwtd6fNsV0DCBpKACwxndLF5mOgBQ3fekS0xGApKEAwBqbKypMR0CKKynbYjoCkDQUAFijSUGh6QhIcc0bNjIdAUgaCgCs0a9rd9MRkOJ+wc8ILEIBgDVOG3C4GuTkmI6BFJWfk6tTDxtoOgaQNBQAWKNpYUNdf9Yw0zGQom48+zxOE8EqFABY5Y/nXaQT+vU3HQMp5sR+h+mWcy8wHQNIKgoArBLw+/XW7fdo+Cmny+/zmY4Dw/w+n353yhl6a+TdCvj5cwi7sPA1rBMKBvXY8Gv16+NP0T8/+0gTZs/U6o0bVF65zXQ0JEFBbp7aNW+how48WJcff5L6dOxsOhJgBAUA1jqgU2c9Ovz3pmMAgBEc8wIAwEIUAAAALEQBAADAQhQAAAAsRAEAAMBCFAAAACxEAQAAwEIUAAAALEQBAADAQhQAAAAsRAEAAMBCFAAAACxEAQAAwEIUAAAALEQBAADAQhQAAAAsRAEAAMBCFABIkrKDQcdjSysqVFFV5XzjsZjzsYBlyiu3qbS83PH4nAR+15FZKACQJOWHQvL7fI7GRqJR3TPqdcUS2JH7KAHAPsViMd376r8UiUYdjQ/4/coPhVxOhXRFFYQkye/zab+GjbR8c6mj8U98/L4mzpujvp26KOB30CuDQcUcFhDABtFoVN8tXaRZRUsdz9GhYSP5+D3DdhQA7NCnRUvHBUCSZq5YppkrlrmYCICb+rRoaToCUginALDDkR06mY4AwENDOvI7jh9RALDDad17mI4AwCM++XRat56mYyCFUACwQ49mzTWgXXvTMQB4YGD79uratKnpGEghFADs5KbDB5uOAMADN/O7jZ+hAGAnQ3v21iGt25iOAcBFh7ZtpzN79DIdAymGAoCd+H0+PX3qmc5u5QOQcoJ+v5465Qxu/8Mu+CuPXfRv2053H3Ws6RgAXPDnY36pfm3amo6BFEQBwG6NHHyULj2or+kYABJwfp8DdesRR5qOgRRFAcBu+Xw+PXf6UA3t2dt0FAAOnN2rj14661zHS3wj81EAsEehQEBvn3uBbhh4hHzijwiQDnzy6abDB+mtc85XKBAwHQcpjAKAvQr4/Xr0hFP03gUXq11hQ9NxAOxF+4YNNebCS/TI8SdzIS/2iWcBoE5O795Tx3Tqoke/nKy/TpuiTZWVpiMB2K5pXp5GHHa4bhw4SA142h/qiAKAOssPhfSnIcfo5sMH6935c/XveXM0fnmRtlRVmY4GWKdRTo6GdOysc3v10Vk9eysvK8t0JKQZCgDqLS8rSxcfeLAuPvBgRaJRFZWWamHJRpVWVqqytsZ0PCBj5Qaz1CQ3T92aNlPnxo05zI+EUACQkIDfr65Nm7LGOACkGeojAAAWogAAAGAhCgAAABaiAAAAYCEKAAAAFqIAAABgIQoAAAAWogAAAGAhCgAAABaiAAAAYCEKAAAAFqIAAABgIQoAAAAWogAAAGAhCgAAABaiAAAAYCEKAAAAFqIAAABgIQoAAAAWCpoOgNRXVl2tT5Ys0ryNG7S+okLRWMx0JAA/4/f51DI/X72bt9CJ+3dTQXa26UhIcRQA7FE4EtF9E8fpf7+crK3hsOk4AOooPxTSzYcP1m2Dj1IoEDAdBymKAoDdKq+u1imvv6RJK5abjgKgnirCYd09fqzGLS/S+xdcytEA7BbXAGC3Lnr3LXb+QJqbsHyZLh09ynQMpCgKAHYxev5cvb9wgekYAFzw3oJ5/D5jtygA2MXfv5pmOgIAFz3x1VTTEZCCKADYSXVtrSatXG46BgAXTVi+TOFIxHQMpBgKAHbyfUWFavhDAWSUcCSideXlpmMgxVAAsJOgnx8JIBNxOyB+jr/22EnL/Hw1CIVMxwDgovxQSC0aNDAdAymGAoCdBP1+ndClq+kYAFx04v7dFODoHn6Gnwjs4g+Dhsgnn+kYAFzgk083HT7YdAykIAoAdtG/bTuNGHC46RgAXHDj4UdoQLv2pmMgBVEAsFuPHH+SfvuLQ03HAJCA4f3666HjTjQdAymKAoDdCvj9eva0s/TvYReqT4uWpuMAqIcDWrbSu+ddpGdOPZNz/9gjHgaEvTq7Vx+d3auPFhRv1NwN61VSuc10JAB70DQ3T31atFL3Zs1MR0EaoACgTno0a64ezZqbjgEAcAnHhgAAsBAFAAAAC1EAAACwEAUAAAALUQAAALAQBQAAAAtRAAAAsBAFAAAAC1EAAACwEAUAAAALUQAAALAQBQAAAAtRAAAAsBAFAAAAC1EAAACwEAUAAAALUQAAALAQBQAAAAtRAAAAsBAFAAAAC1EAAACwEAUAAAALUQAAALAQBQAAAAtRAAAAsBAFAAAAC1EAAACwEAUAAAALUQAAALAQBQAAAAtRAAAAsBAFAAAAC1EAAACwEAUAAAALUQAAALAQBQAAAAtRAAAAsBAFAAAAC1EAAACwEAUAAAALUQAAALAQBQAAAAtRAAAAsBAFAAAAC1EAAACwEAUAAAALUQAAALAQBQAAAAtRAAAAsBAFAAAAC1EAAACwEAUAAAALUQAAALAQBQAAAAtRAAAAsBAFAAAAC1EAAACwEAUAAAALUQAAALAQBQAAAAtRAAAAsBAFAAAAC1EAAACwEAUAAAALUQAAALAQBQAAAAtRAAAAsBAFAAAAC1EAAACwEAUAAAALUQAAALAQBQAAAAtRAAAAsBAFAAAAC1EAAACwEAUAAAALUQAAALAQBQAAAAtRAAAAsBAFAAAAC1EAAACwEAUAAAALUQAAALAQBQAAAAtRAAAAsBAFAAAAC1EAAACwEAUAAAALUQAAALAQBQAAAAtRAAAAsBAFAAAAC1EAAACwEAUAAAALUQAAALAQBQAAAAtRAAAAsBAFAAAAC1EAAACwEAUAAAALUQAAALAQBQAAAAtRAAAAsBAFAAAAC1EAAACwUNB0AEgrt2zW4pISbarcpi3VVaoIh3d8lVZWmo4HABlj0srlpiOkDApAEoUjEU1bvVKTVizX3I0btKikWAuLN6oiHDYdDQBgGQqAx75bt1Zji5Zq7LIlmrxyhbayswcApAAKgAfWlpdp1Nw5enH6N5q1/nvTcQAA2AUFwCW10ajemT9XL07/RmOLlioai5mOBADAHlEAElQTjWrUorl6ZPqXWlRSbDoOAAB1QgFwKByJ6JX5s/Tod1O1omyL6TgAANQLBcCByWtWasT4T7SwlE/8AID0RAGoh9KqSt315Xj9c+50cYYfAJDOKAB19NK8mRo5Zaw2V1eZjgIAQMIoAPuwtSas34/7WKMWzTUdBQAA11AA9mJRaYku/uRdzSvZaDoKAACuogDswZsL5+i6cR9rW22N6SgAALiOArAbT838Wn+Y9F8u9AMAZCwKwE/EJD3w1STd/9Uk01EAAPAUBWC7SDSm68Z9pH/OnWFk+61z8tSpQYHa5uSpIJilvGBQeYGgCoJZRvIAQCYas26lvi7lui6JArDDGwtnqzoSScq28gJB9W/cXIc1aa7+jZura36hcgP8XwEAXptbVkoB2I69znZe7/xzAwH9skVbndmmgw5v0lIBn8/T7QEAsDcUAI91yivQbzp11ykt26tBkLcbAJAa2CN5pEdBI/2uUw+d1LKd/HzaBwCkGAqAyxpmhXRtl166pP3+7PgBACmLAuASn6Sz23bSrd0OUOOsbNNxAADYKwqACxoEgrq/1y90cuv9TEcBAKBOKAAJ6tWgoR7q3FfdWjQ1HQUAgDqjACTg6EYt9UDnvgr5/aajAABQLxQAh85tvp/+0KG3/OJCPwBA+qEAOHB5qy66tl130zEAAHCMY9f1NLT5fuz8AQBpjwJQD0MatdRt+/U2HQMAgIRRAOqoW16hHurclzX8AQAZgQJQB3n+gB7sfDBX+wMAMgZ7tDq4rUMfdczJNx0DAADXUAD24ZjGrXRK07amYwAA4CoKwF7k+AO6sV1P0zEAAHAdBWAvhrfpqjbZuaZjAADgOhYC2oOWoRxd2LJjUrcZicW0oHyzFleUaVXlVlVGalVeW5PUDACQyb4uLTYdIWVQAPbg0ladleXz/gBJTNLUkvV6Z+1yTdi4TmXs8AEASUAB2I0mwZDOatbe8+18sXGtHlsyVwvKN3u+LQAAfooCsBvDWnRQjj/g2fybwtUaOe8bfb5hrWfbAAC4bovpAG6iAPyMT9KpTdt5Nv/sslJdNX2K1ldXerYNAIAn1pgO4CYKwM8cUtDEsyv/vyrdqCunT9bW2lpP5gcAeKrIdAA3cRvgz3i16M+88s0aPn0KO38ASE8zlGFHACgAP+GTNKhhC9fnLaut0TUzpqqCK/wBIF2NNh3AbRSAn+iYk69mWdmuz/uXBdO1unKr6/MCAJKiTNJTpkO4jQLwE4cWNnV9zm83F+u9tStcnxcAkDQPScq4FYQoAD/RN7+x63M+sXSeYq7PCgBIkvGSHjYdwgsUgJ/o5PIjfxdXlGlKyXpX5wQAJM0SSedKysgLuCgA2/nl0345DVyd8711HPoHgDQ1QdJAZeCh/x9QALZrGcpxffW/icXrXJ0PAOC5ckl/kvRLZfDOX2IhoB1ahnJcna+stkYLyzNq1UgAyGRzJL0n6QlJGwxnSQoKwHZ5AXffiqKtZQld/NewUSMNu+hSdejcWT6fb5/fnxsMqA7fBgBpY87sWXrh2WedDp+pfd+6V6n44j5FkpY73VC6ogBsl+fy4f91Vc7X+s/Ly9O7n45Vpy771+n7/T6pMCfL8fYAIBV9/OGHiRSAFZIcD7YB1wBs5/YRgG0JLPk7+Ohj67zzlySf+OgPAKgfCsB2WT5334qaWNTx2KbNmtVvAPt/AEA9UQAAALAQBQAAAAtRAAAAsBAFAAAAC1EAAACwEAUAAAALUQAAALAQBQAAAAtRAAAAsBAFAAAAC1EAAACwEAUAAAALUQAAALCQu8/ARcrasmWLJo4bp9WrVilcEzYdB7BSTk6uunbrpiMGDVJ2To6RDLFYTDNnzND0b7/VhvXrVVVVZSRHXRQVLTUdIaNRADJccXGx7r/3Hr328suqqakxHQeApMLCQv3++ut17fU3KDs7O2nb/fD993XPnXdqyeJFSdsmUhenADLYwgXzdezgQfrXCy+w8wdSSFlZme6/916dcfJJ2rRpU1K2edfIkbrk/PPY+WMHCkCGKikp0bCzztKqlStNRwGwB19Nm6bLL7pIkUjE0+38/a+P64nHH/N0G0g/FIAMdf+997DzB9LApIkT9Porr3g2/6qVK3XfPfd4Nj/SFwUgA5WXl3n6BwWAu5564gnP5v7n88+rurras/mRvigAGWji+An8wgNpZOGC+Vq9apUnc4/972eezIv0RwHIQCtXrDAdAUA9rVi+PK3mRfqjAGQgn89nOgKAevL7vflzzN8D7AkFIAPt16GD6QgA6qn9fvt5Mi9/D7AnFIAMNHjIkUldXARAYnr07Kl27dt7Mvdxx5/gybxIfxSADFRQUKhLfvUr0zEA1NE1I0Z4Nvdvhg9Xbl6eZ/MjfVEAMtQf77iTQ39AGhhy1NE6/8KLPJu/dZs2uuNPd3k2P9IXBSBDNWnSRG+PHk0JAFLYYQMH6sVXX1UgEPB0O1dde61uuPkWT7eB9EMByGDduvfQF5On6Iorr1QoFDIdB8B2DRs21B13363/fPSxGjdunJRt3nnPPXrznXfVo2fPpGwPqY+nAWa4Jk2a6OHHHted99yriePHa9WKFdqwYUNStv3BmP9o6ZIljsYeesxQtWrftV5jCs08XdWY9996TmWbnT1I5tLLL1fjxk1cToS9CQQCat6ihbr36K6Bf5IJVQAAD+5JREFURwwycqHu8SeeqONPPFFzZs/WjO++VUlJSdIz1MeiBQv1xmuvmo6RsSgAligsLNSpp5+e1G0uWbzIcQEYdPIl+sVRZ9ZrTIfkfJBKGRM/G+24AFw74np16Vq/goXM0eeAA9TngANMx9injz/8kALgIU4BAABgIQoAAAAWogAAAGAhCgAAABaiAAAAYCEKAAAAFqIAAABgIQoAAAAWogAAAGAhCgAAABaiAAAAYCEKAAAAFqIAAABgoUwrAGGnA2ti0QQ37dvpn7J8zt/akuLi+g2IOd4UAMBSmVYAyp0O3BapTWjDsZ/thfOCzp+0PPGLz1W0ZHHdtx2jAQAA6sf5Xio1OS8A0UhiW47GpMCP/9gmJ8/xVJWVlRp6wrE696JL1KnL/vL5fPsckxv01+n7kmnlihWmI2AP3n3n32rRoqXpGMBezZk9K5HhHSRduY/vqZK0TtJSSUWJbCwdZVoBqHA6MPEjADvr1KBAfp9PUYefzsu2bNELT/09oUzAnjzw5z+bjgB47SBJ/6jH9y+Q9K6kJyR970miFMMpgO2+D1cltOFYdOcdfWEwS93yGyY0JwAgaXpIul3SYkl3SwoZTZMEmVYASp0OXB+uVFUCpwFi0V0vIhzSrJXj+QAARuRLukvS55JaGM7iqUwrAGskbXMyMCZpRdVWxxuORHY91H9m6w6O5wMAGDVY0lRJzU0H8UqmFYCo4odvHFlW5fgSAsUiux4B2D+/UIOacqEVAKSpLpLeUYaeDsi0AiBJC50OnF6+yfFGI7s5BSBJ13bprdS6Nh8AUA+DJf3BdAgvUAB+4usECoBiUnQ3RwEOadRUQ9t2dD4vAMC0W5SB1wNkYgGY73Tg8qoKbaxxfjdApHb3FxGO7H6w9svLdzwvAMCoAklXmw7htkwsAJMTGrxlo+OxtTW7Pw1QEMzSkwcdroJgluO5AQBGnWU6gNsysQCsUgIXAn5Yssbxhvd0BECSehQ01LN9BymfEgAA6ehASe1Mh3BTJhYASfrC6cDp5Zu0ptrRnYSKRWO7vQ7gB/0aN9Nrhx6l1gksEwwAMKaz6QBuytQCMM7pwJikDxI4ClBTvfclhXsVNNKYgb/UCS0zqkgCgA3amA7gpkwuAI6f7ztqwwrHqwLWhGv3+XTeRlkh/f2ggXr+kMHqXdjY0XYAAEmXUeu7Z2oB2KAETgNsqg3r3Y2rHI2NRWOK1tStPAxp1krvDThOL/cborPadFCjrIxcawIAkIIy7WmAP/WKpOMcD15fpHNa7KeQr/4dKRyuVW5WYN/fuN3AJi00sEkLRWMxLarYoiVby7Ry21ZtjdSqrCZc7+2nikkl67Wm0vnyyvDOKa3ac1cKrPR1abGWbi0zHSMlZHIBeEfSk4o/2KHe1oer9Nr6Zbq8VZd6j62tjiiWG5PPX781AP0+n3oUNFKPgkb13mYqunrGVApAihqxf291yiswHQNIujvnfUsB2C5TTwFI0lZJoxOZ4Lm1SxzeERBTdVVNIpsGAMBTmVwAJOnFRAZXRSN6dJWzhQVrqmsUi+7rckAAAMzI9AIwXvHHOTo2bvN6Z7cFxsRRAABAysr0AiBJDyY6wQMr5jh6VHBNVc1eFwYCAMAUGwrAB5JmJjJBZTSiPyydrmoHawNUb03fq/gBAJnLhgIQk/RAopMsqSzXrUunKxKr33n92trIPlcHBAAg2WwoAJI0StLXiU4yacsG3bdizj5X+vu56m1hxepZHAAA8JItBSCq+LOcEz4h/17xKv1t9YJ6jYnFYqosr0500wAAuMaWAiBJ30j6hxsTvfR9ke5ePqtepwMitRFVV3I9AAAgNdhUACTpNknr3JhoTPFq3bL0u3pdGBiuqlFtHZ8TAACAl2wrAFsk3ejWZOM3r9evF0zT6rquFhiTKiuqFKnl1kAAgFm2FQBJelPSC25NNn/bFl0wb7I+21THAwsxqbK8SjHWBwAAGGRjAZCkayXNdmuyrZFa/bFouv60bKY21e77PH8sFtO28iqWCgYAGGNrAaiUdLakcjcn/aBkjc6aPUGvr1+u6D4uEIxGY9paVqkoJQAAYICtBUCSFku6Uqr3bf17VR6p0SOr5um8eZP0UcnavRaBWDSmyrJKlgsGACSdzQVAil8PcJMXEy+trNAdy2bozDkT9M7GlaqI7H41wGg0pm1lVYpwdwAAIIlsLwCS9Jikh7yafHX1Nt23Yo6Om/m5/lg0XZO2bFA4tvMn/h+uCaiuDLt8PAIAgN0Lmg6QIm6T1ELS5V5tIByN6rNN6/TZpnXK8Qd0cH5jHVrYVP0KmqpzTr4aBIIKV9YoWhNVdn62/H6fV1EAAKAAbBdT/HqAgKRLvd5YVTSiaWXFmlZWvOPfNc/KUcecBmqTnasGgSwV5oRUEAqpMCvkdRzPrK7cajpCwjZv2qg5303VpuL1CmXnqEv3A9Slx0Hy+zl45sT66krN3LJJm8IsjQ0zlm519drvtEYB+FGtpF9J2iDp5mRvfGNNlTbWVLl8XwKcWr1iiZ75nz9q0mejFY3ufMqmdftOuuyaO3TyOZfL5+NITV3MLivVI4tm6ctNGzjLBaQIPsbsLCbplu1f/J2y1LiPRunXp/bVhE/e2WXnL0nrVi3Tg3+8Qrf/7kxt49PEPr21ukjD/u8LTWXnD6QUCsDuPaL4qYBK00GQXC8/eZ/uuu48VW6r2Of3Tv58jK4eNkjr165MQrL09Nn6Nbpz3reqjXGrKzLCvv8wpBEKwJ69KukXkuaYDgLv1dSEdf8tv9Jzj96hWD2e8rh0wSxdcfovNOubyR6mS0/bIrW6a/53fOpHJlljOoCbKAB7N1/SQMXLADLUltJi3XDJcfr43Zccj7/xsuP1xYdvu5wsvb2zZrmKw1WmYwBuyqjDfRSAfauQdImkKxR/miAyyIqlCzR86ADN/HpSQvNUV1Xq7hHn6+Un76vXEYRMNqHYlSdvA6liiaQi0yHcRAGouxcl9ZD0mrhAMCN8M+Vz/e6cgVqzcqkr88ViMT336B2675bLVMNtblqZAbeBAj/xnukAbuM2wPr5XtLFkp6X9KSkXmbjwKkxbz6rx+76vWpra1yf+9PRr2jdqmW6/5nRati4mevzu+H+BTMPfemwo91pPnuwYlvFN5I6e7kNIEmqJT1lOoTbKADOjJd0sKTfSrpVUgejaVBn0UhETz10q9564VFPtzPrm8kaPnSAHnrufXXYv6en23JifPG6sg4fvl7q8WaWiwKAzPC0pGWmQ7iNUwDO1SjeCLtK+rXiTxdECqvcVqGRVw/1fOf/gzUrl+p35x6ub6Z8npTtpaDPTAcAXDBP0l2mQ3iBApC4Gkn/lNRT0vmSxkripucUs/H71brmvMGa/PmYpG63omyzbr78JI1589mkbjdF/FOsbYn0ViLpdEllpoN4gQLgnoiktyQdp/gpgdsUb44wbMHsb/Tbs/pr8bwZRrYfidTq4ZHD9cR9NyoaseqxzxsU/z0A0tEiSYMkeXqtjEkUAG+slvSgpN6S+ip+ncAnyrBVpNLBhE/e0bUXDFHJBvO3pL394mO6/aqz6rTKYAZ5UtIDpkMA9RCV9LKkAZIWGM7iKQqA92ZIeljSSZKaKN4oR0p6XdK34hCpZ1595kH96dphqqrcZjrKDlPGvq+rhw3ShnWrTEdJptslDVX8PmogVZVLelPxFWAvk+T1RbLGcRdActVImrL966faKn4xYXNJhZLyt38VSGqczIAuO15Sx2RvtKYmrIdHDtfH7/wr2ZuukyXzZ+rKoYfpgX/8Rz0PPNR0nGQZLek/iq+sOVDxn/k8o4lgu5ik9ZLWKV5OJyl+u581KACpYY0ybI3p7UYryQWgbPMm3XnNOfpu2rhkbrbeSjas0+/PP1K3PfSijjvtAtNxkiWq3RdgAAZwCgAZY2XRQg0/e4BXO3/Xr94LV1fp3hsu0itPc4ocQPJRAJARFs6YrKuHDdLq5Z4sx1Ak6SBJ97g9cSwW07OP3K67R1ygcDUPzgGQPBQApL0JY17UA1cdqy2lxV5MP1Xxc9ZzJd0t6TeKX8vhqrEfvKkRFx+rzZs2uj01AOwWBQBpKxaL6d1n79Zz916h2pqwF5t4S9Kxit/P/oMXFL+jw/UrhOd8N1W/O3ugViyZ7/bUALALCgDSUnXlVj1+y1C9+6zrR+Wl+NXB90i6QNLujsuPldRf0kK3N7xm5VJdNewIffflF25PDQA7oQAg7ZRuXKu/DD9K34735Omc1Yo/8fFu7f2xz0skHa74g6FcVb6lVDdedoJGv5pxDx8DkEIoAEgrKxfP0t2XD9Syed94Mf33ko5UfJGmutgk6QRJL7kdJBKp1aN3XaO/3jtC0SiPlgDgPgoA0sbMqR/r3isGqeT7lV5MP1vxpT+/que4sKRfSbpeHjwE6t8v/U1/+M2p2lqRkc8iAWAQBQBp4dM3/qpHrj9VVds8WTn5U0mDJa1IYI6/ShomyfV1h6dN+FjXnDdY69d6UnwAWIoCgJQWidTqXw9erVf+93rFvDkU/qykUyVtcWGudyQdofjDoFy1dMEsDT97gBbM9uTUBwALUQCQsraWleqh35+gz//9tBfTRyRdJ2m4pFoX552h+KmE71ycU1J8+eBrzhussR+86fbUAAC4arTiV9LX++vC6x+JtenYw9HYOnyVSTrF01cef5jTf7zI7/P5Ypdfd1esXceuiczTzdNXDwCwmuMC4PP5vNr5L5XUy9NX/aOApMc8eh2JvkcUAACAZxwXAI++vpTU0tNXvHu/UfxuAdOvnwIAYAeuAYAt3pZ0jOLP/0625yWdLGmzgW0DwG5RAJDpYpIeknS+pEqDOT5XfPngRQYzAACQFKZPAVQpvqxvKmmq+PLBnAIAAGSsd2RuB/e94rfjpaJsSa/IbAHo4vmrBABY6wWZ2bnNltTR+5eXsBGKr0dg4j1qnITXBwCw1J+V/B3bR5IKk/HiXHKe4tcmJPM9qhDX/wAAPHSakrtj+4ekYFJembsOk7ROyXufxiflVQEArFUoaau836HVSLomSa/JKx0kzVJyCsAfkvSaAAAW8/pity2STkzaq/FWoeKnMLx8v8KS2ibrBQEA7NVN3q2Ct0rSwcl7KUkRkPSgvCsAf0/eSwEA2M6LHdpUSS2S+SKS7BrFT224+Z6tldQ8mS8CAGC3LEkT5N6O7A1JOUl9BWacoPgpDjfes2pJRyU1PQAAip/fnqLEdmJRSXdL8iU3ulG9JS1TYu9bleJ3ZAAAYESOpKfkbCf2vaRTkh85JTSVNEbO3relkvolPzIAALsapPg5/LrswLZK+psy+3x/XfgkDZO0UHV730ol3Skpz0RYAKnPpkOpSD19JZ0tabCk7pKaKL4q3jrFl/P9XPEHCm0wFTAF+SUdKWmo4gsIdZbUSFK5pDWSvpH0maT3ZPbphwBS3P8DkrYAzQqyPl4AAAAASUVORK5CYII=',
        ),
      ),
    ),
    'objets' => 
    array (
      0 => 
      array (
      ),
    ),
  ),
);
