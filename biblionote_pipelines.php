<?php
/**
 * Utilisations de pipelines par Biblionote
 *
 * @plugin     Biblionote
 * @copyright  2021
 * @author     Fa_b
 * @licence    GNU/GPL v3
 * @package    SPIP\Biblionote\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}



/**
 * Ajout de contenu sur certaines pages,
 * notamment des formulaires de liaisons entre objets
 *
 * @pipeline affiche_milieu
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function biblionote_affiche_milieu($flux) {
	$texte = '';
	$e = trouver_objet_exec($flux['args']['exec']);



	// biblionotes sur les mots
	if (
		$e
		and !$e['edition']
		and in_array($e['type'], array('mot'))
	) {
		$texte .= recuperer_fond('prive/objets/editer/liens', array(
			'table_source' => 'biblionotes',
			'objet' => $e['type'],
			'id_objet' => $flux['args'][$e['id_table_objet']]
		));
	}

	if ($texte) {
		if ($p = strpos($flux['data'], '<!--affiche_milieu-->')) {
			$flux['data'] = substr_replace($flux['data'], $texte, $p, 0);
		} else {
			$flux['data'] .= $texte;
		}
	}

	return $flux;
}




/**
 * Optimiser la base de données
 *
 * Supprime les liens orphelins de l'objet vers quelqu'un et de quelqu'un vers l'objet.
 * Supprime les objets à la poubelle.
 *
 * @pipeline optimiser_base_disparus
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function biblionote_optimiser_base_disparus($flux) {

	include_spip('action/editer_liens');
	$flux['data'] += objet_optimiser_liens(array('biblionote'=>'*'), '*');

	sql_delete('spip_biblionotes', "statut='poubelle' AND maj < " . $flux['args']['date']);

	return $flux;
}
