<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin Biblionote
 *
 * @plugin     Biblionote
 * @copyright  2021
 * @author     Fa_b
 * @licence    GNU/GPL v3
 * @package    SPIP\Biblionote\Installation
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Fonction d'installation et de mise à jour du plugin Biblionote.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
**/
function biblionote_upgrade($nom_meta_base_version, $version_cible) {
	$maj = array();

	$maj['create'] = array(array('maj_tables', array('spip_biblionotes', 'spip_biblionotes_liens')));
	$maj['1.0.2'] = array(array('sql_alter','TABLE spip_biblionotes CHANGE titre titre VARCHAR(128) NOT NULL DEFAULT ""'));

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}


/**
 * Fonction de désinstallation du plugin Biblionote.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
**/
function biblionote_vider_tables($nom_meta_base_version) {

	sql_drop_table('spip_biblionotes');
	sql_drop_table('spip_biblionotes_liens');

	# Nettoyer les liens courants (le génie optimiser_base_disparus se chargera de nettoyer toutes les tables de liens)
	sql_delete('spip_documents_liens', sql_in('objet', array('biblionote')));
	sql_delete('spip_mots_liens', sql_in('objet', array('biblionote')));
	sql_delete('spip_auteurs_liens', sql_in('objet', array('biblionote')));
	# Nettoyer les versionnages et forums
	sql_delete('spip_versions', sql_in('objet', array('biblionote')));
	sql_delete('spip_versions_fragments', sql_in('objet', array('biblionote')));
	sql_delete('spip_forum', sql_in('objet', array('biblionote')));

	effacer_meta($nom_meta_base_version);
}
