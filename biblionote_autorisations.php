<?php
/**
 * Définit les autorisations du plugin Biblionote
 *
 * @plugin     Biblionote
 * @copyright  2021
 * @author     Fa_b
 * @licence    GNU/GPL v3
 * @package    SPIP\Biblionote\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Fonction d'appel pour le pipeline
 * @pipeline autoriser */
function biblionote_autoriser() {
}


// -----------------
// Objet biblionotes


/**
 * Autorisation de voir un élément de menu (biblionotes)
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_biblionotes_menu_dist($faire, $type, $id, $qui, $opt) {
	return true;
}


/**
 * Autorisation de voir le bouton d'accès rapide de création (biblionote)
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_biblionotecreer_menu_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('creer', 'biblionote', '', $qui, $opt);
}

/**
* Autorisation de voir (biblionotes)
*
* @param  string $faire Action demandée
* @param  string $type  Type d'objet sur lequel appliquer l'action
* @param  int    $id    Identifiant de l'objet
* @param  array  $qui   Description de l'auteur demandant l'autorisation
* @param  array  $opt   Options de cette autorisation
* @return bool          true s'il a le droit, false sinon
**/
function autoriser_biblionotes_voir_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

/**
* Autorisation de voir (biblionote)
*
* @param  string $faire Action demandée
* @param  string $type  Type d'objet sur lequel appliquer l'action
* @param  int    $id    Identifiant de l'objet
* @param  array  $qui   Description de l'auteur demandant l'autorisation
* @param  array  $opt   Options de cette autorisation
* @return bool          true s'il a le droit, false sinon
**/
function autoriser_biblionote_voir_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

/**
 * Autorisation de créer (biblionote)
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_biblionote_creer_dist($faire, $type, $id, $qui, $opt) {
	return in_array($qui['statut'], array('0minirezo', '1comite'));
}

/**
 * Autorisation de modifier (biblionote)
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_biblionote_modifier_dist($faire, $type, $id, $qui, $opt) {
	return in_array($qui['statut'], array('0minirezo', '1comite'));
}

/**
 * Autorisation de supprimer (biblionote)
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_biblionote_supprimer_dist($faire, $type, $id, $qui, $opt) {
	return $qui['statut'] == '0minirezo' and !$qui['restreint'];
}



/**
 * Autorisation de lier/délier l'élément (biblionotes)
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_associerbiblionotes_dist($faire, $type, $id, $qui, $opt) {
	return $qui['statut'] == '0minirezo' and !$qui['restreint'];
}
