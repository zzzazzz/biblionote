<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'biblionote_description' => 'Pour éviter d\'installer un plugin trop lourd de type zotero, et pouvoir afficher comme pour un blog, une liste de références (avec ou sans lien). Pensez à augmenter ce plugin avec les mots-clés.',
	'biblionote_nom' => 'Biblionote',
	'biblionote_slogan' => 'Pour créer des listes bibliographiques simples',
);
