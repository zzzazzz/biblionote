<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_biblionote' => 'Ajouter cette biblionote',

	// C
	'champ_annee_explication' => 'Année de l\'édition de l\'ouvrage',
	'champ_annee_label' => 'Annee',
	'champ_auteur_explication' => 'Ex: Lorentz (Philippe) et Sandron (Dany)',
	'champ_auteur_label' => 'Auteur(s)',
	'champ_commentaire_label' => 'Commentaire',
	'champ_editeur_explication' => 'Nom de l\'éditeur',
	'champ_editeur_label' => 'Éditeur',
	'champ_lien_explication' => 'Lien vers la ressource (site, éditeur…)',
	'champ_lien_label' => 'Lien',
	'champ_lieu_explication' => 'Concerne principalement l\'édition d\'ouvrages',
	'champ_lieu_label' => 'Lieu de la publication',
	'champ_titre_explication' => 'Titre de l\'ouvrage, site, magazine',
	'champ_titre_label' => 'Titre',
	'confirmer_supprimer_biblionote' => 'Confirmez-vous la suppression de cette biblionote ?',

	// I
	'icone_creer_biblionote' => 'Créer une biblionote',
	'icone_modifier_biblionote' => 'Modifier cette biblionote',
	'info_1_biblionote' => 'Une biblionote',
	'info_aucun_biblionote' => 'Aucune biblionote',
	'info_biblionotes_auteur' => 'Les biblionotes de cet auteur',
	'info_nb_biblionotes' => '@nb@ biblionotes',

	// R
	'retirer_lien_biblionote' => 'Retirer cette biblionote',
	'retirer_tous_liens_biblionotes' => 'Retirer toutes les biblionotes',

	// S
	'supprimer_biblionote' => 'Supprimer cette biblionote',

	// T
	'texte_ajouter_biblionote' => 'Ajouter une biblionote',
	'texte_changer_statut_biblionote' => 'Cette biblionote est :',
	'texte_creer_associer_biblionote' => 'Créer et associer une biblionote',
	'texte_definir_comme_traduction_biblionote' => 'Cette biblionote est une traduction de la biblionote numéro :',
	'titre_biblionote' => 'Biblionote',
	'titre_biblionotes' => 'Biblionotes',
	'titre_biblionotes_rubrique' => 'Biblionotes de la rubrique',
	'titre_langue_biblionote' => 'Langue de cette biblionote',
	'titre_logo_biblionote' => 'Logo de cette biblionote',
	'titre_objets_lies_biblionote' => 'Liés à cette biblionote',
	'titre_page_biblionotes' => 'Les biblionotes',
);
