<?php
/**
 * Déclarations relatives à la base de données
 *
 * @plugin     Biblionote
 * @copyright  2021
 * @author     Fa_b
 * @licence    GNU/GPL v3
 * @package    SPIP\Biblionote\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Déclaration des alias de tables et filtres automatiques de champs
 *
 * @pipeline declarer_tables_interfaces
 * @param array $interfaces
 *     Déclarations d'interface pour le compilateur
 * @return array
 *     Déclarations d'interface pour le compilateur
 */
function biblionote_declarer_tables_interfaces($interfaces) {

	$interfaces['table_des_tables']['biblionotes'] = 'biblionotes';

	return $interfaces;
}


/**
 * Déclaration des objets éditoriaux
 *
 * @pipeline declarer_tables_objets_sql
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function biblionote_declarer_tables_objets_sql($tables) {

	$tables['spip_biblionotes'] = array(
		'type' => 'biblionote',
		'principale' => 'oui',
		'field'=> array(
			'id_biblionote'      => 'bigint(21) NOT NULL',
			'titre'              => 'varchar(128) NOT NULL DEFAULT ""',
			'lien'               => 'tinytext NOT NULL DEFAULT ""',
			'auteur'             => 'varchar(25) NOT NULL DEFAULT ""',
			'editeur'            => 'varchar(25) NOT NULL DEFAULT ""',
			'lieu'               => 'varchar(25) NOT NULL DEFAULT ""',
			'annee'              => 'varchar(25) NOT NULL DEFAULT ""',
			'commentaire'        => 'mediumtext NOT NULL DEFAULT ""',
			'statut'             => 'varchar(20)  DEFAULT "0" NOT NULL',
			'maj'                => 'TIMESTAMP NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'
		),
		'key' => array(
			'PRIMARY KEY'        => 'id_biblionote',
			'KEY statut'         => 'statut',
		),
		'titre' => 'titre AS titre, "" AS lang',
		 #'date' => '',
		'champs_editables'  => array('titre', 'lien', 'auteur', 'editeur', 'lieu', 'annee', 'commentaire'),
		'champs_versionnes' => array('titre', 'lien', 'auteur', 'editeur', 'lieu', 'annee', 'commentaire'),
		'rechercher_champs' => array("titre" => 6, "lien" => 4, "auteur" => 7, "editeur" => 4, "lieu" => 2, "annee" => 2, "commentaire" => 5),
		'tables_jointures'  => array('spip_biblionotes_liens'),
		'statut_textes_instituer' => array(
			'prepa'    => 'texte_statut_en_cours_redaction',
			'prop'     => 'texte_statut_propose_evaluation',
			'publie'   => 'texte_statut_publie',
			'refuse'   => 'texte_statut_refuse',
			'poubelle' => 'texte_statut_poubelle',
		),
		'statut'=> array(
			array(
				'champ'     => 'statut',
				'publie'    => 'publie',
				'previsu'   => 'publie,prop,prepa',
				'post_date' => 'date',
				'exception' => array('statut','tout')
			)
		),
		'texte_changer_statut' => 'biblionote:texte_changer_statut_biblionote',


	);

	return $tables;
}


/**
 * Déclaration des tables secondaires (liaisons)
 *
 * @pipeline declarer_tables_auxiliaires
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function biblionote_declarer_tables_auxiliaires($tables) {

	$tables['spip_biblionotes_liens'] = array(
		'field' => array(
			'id_biblionote'      => 'bigint(21) DEFAULT "0" NOT NULL',
			'id_objet'           => 'bigint(21) DEFAULT "0" NOT NULL',
			'objet'              => 'VARCHAR(25) DEFAULT "" NOT NULL',
			'vu'                 => 'VARCHAR(6) DEFAULT "non" NOT NULL',
		),
		'key' => array(
			'PRIMARY KEY'        => 'id_biblionote,id_objet,objet',
			'KEY id_biblionote'  => 'id_biblionote',
		)
	);

	return $tables;
}
